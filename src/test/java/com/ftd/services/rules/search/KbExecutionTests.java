package com.ftd.services.rules.search;

import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.api.Status;
import com.ftd.services.rules.search.bl.impl.KnowledgeBaseBuilder;
import com.ftd.services.rules.search.bl.repository.RuleReloadTriggerRepository;
import com.ftd.services.rules.search.bl.repository.RuleRepository;
import com.ftd.services.rules.search.config.AppConfig;
import com.ftd.services.search.api.request.SearchServiceRequest;
import com.ftd.services.search.api.response.SearchServiceResponse;
import com.ftd.services.search.api.rules.RuleServiceResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class KbExecutionTests {

    @Test
    public void reloadAndFireRules() throws Exception {

        final AppConfig appConfig = Mockito.mock(AppConfig.class);
        final List<String> statusList = new ArrayList<>();
        statusList.add(Status.ACTIVE.name());
        when(appConfig.getRuleStatusList()).thenReturn(statusList);

        final RuleRepository rr = Mockito.mock(RuleRepository.class);
        final List<RuleEntity> rules = new ArrayList<>();
        final RuleEntity r = new RuleEntity();

        final StringBuilder sb = new StringBuilder();
        sb.append("package junit ");
        sb.append("import com.ftd.services.search.api.request.SearchServiceRequest; ");
        sb.append("rule \"jUnit1\"");
        sb.append("dialect \"java\" ");
        sb.append("when ");
        sb.append("    req : SearchServiceRequest()");
        sb.append("then");
        sb.append("    req.setQ(\"WHAT!\");");
        sb.append("end");
        r.setRule(sb.toString());

        rules.add(r);
        when(rr.findByStatus(Status.ACTIVE)).thenReturn(rules);

        final RuleReloadTriggerRepository rtr = Mockito.mock(RuleReloadTriggerRepository.class);

        final KnowledgeBaseBuilder kb = new KnowledgeBaseBuilder(appConfig, rr, rtr);
        kb.reloadRules();

        final RuleServiceResponse response = new RuleServiceResponse();
        response.setSearchServiceRequest(new SearchServiceRequest());
        response.setSearchServiceResponse(new SearchServiceResponse());

        kb.fireRules(response);
        Assert.assertEquals("WHAT!", response.getSearchServiceRequest().getQ());
    }

}
