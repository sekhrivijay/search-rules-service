package com.ftd.services.rules.search.controller;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.rules.search.bl.RulesExecutionService;
import com.ftd.services.search.api.rules.RuleServiceResponse;
import com.services.micro.commons.logging.annotation.LogExecutionTime;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RefreshScope
@RequestMapping("/{siteId}/api/rules")
public class RulesExecutionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RulesExecutionController.class);

    private RulesExecutionService rulesExecutionService;

    public RulesExecutionController(@Autowired final RulesExecutionService rulesExecutionService) {
        this.rulesExecutionService = rulesExecutionService;
    }

    @Timed
    @ExceptionMetered
    @PostMapping("/executePre")
    @LogExecutionTime
    @ApiOperation(
            value = "Execute the AI rules against the search request",
            notes = "The rules are applied (fired) on the request before it is sent to SOLR",
            response = RuleServiceResponse.class)
    public RuleServiceResponse executePre(
            @PathVariable final String siteId,
            @RequestBody final RuleServiceResponse searchModelWrapper) {
        LOGGER.debug(searchModelWrapper.toString());
        return rulesExecutionService.executePre(searchModelWrapper);
    }

    @Timed
    @ExceptionMetered
    @PostMapping("/executePost")
    @LogExecutionTime
    @ApiOperation(
            value = "Execute the AI rules against the search request / reponse",
            notes = "The rules are applied (fired) on the "
                    + "request / response after it has been sent to SOLR",
            response = RuleServiceResponse.class)
    public RuleServiceResponse executePost(
            @PathVariable final String siteId,
            @RequestBody final RuleServiceResponse searchModelWrapper) {
        LOGGER.debug(searchModelWrapper.toString());
        return rulesExecutionService.executePost(searchModelWrapper);
    }
}
