package com.ftd.services.rules.search.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.bl.RulesService;
import com.google.gson.Gson;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * This class provides a restful API to maintain the drools rules. The most
 * interesting thing to note is that the rule attribute in the RuleEntity is
 * encoded to base64 during transit. This is because JSON does not handle the
 * drools rule formats very well and this was the "workaround".
 *
 */
@Api
@RestController
@RefreshScope
@RequestMapping("/api/rules")
public class RulesImportController {

    private static final Logger   LOGGER = LoggerFactory.getLogger(RulesImportController.class);

    private RulesService rulesService;

    public RulesImportController(@Autowired RulesService rulesService) {
        this.rulesService = rulesService;
    }

    private void decodeRuleFromTransport(RuleEntity ruleEntity) {
        if (ruleEntity == null || ruleEntity.getRule() == null) {
            return;
        }
        try {
            ruleEntity.setRule(new String(Base64.getDecoder().decode(ruleEntity.getRule())));
        } catch (Exception e) {
            LOGGER.warn("base64 encoding error on request", e.getMessage());
        }
    }

    @PostMapping("/importRuleFile")
    @ApiOperation(
            value = "Import a file of rules",
            notes = "Replace the entire database of rules with the contents of a file")
    public void importRuleFile(@RequestParam("file") MultipartFile multipartFile) {
        try {
            byte[] bytes = multipartFile.getBytes();
            Gson gson = new Gson();
            RuleEntity[] rules = gson.fromJson(new String(bytes), RuleEntity[].class);
            rulesService.importRules(rules);
        } catch (IOException e) {
            throw new InputValidationException(HttpStatus.BAD_REQUEST.name(), e);
        }
    }

    @PostMapping("/importRules")
    @ApiOperation(
            value = "Import an array of rules",
            notes = "Replace the entire database of rules with the array of rules")
    public void importRules(@RequestBody RuleEntity[] encodedRules) {
        Arrays.stream(encodedRules).forEach(this::decodeRuleFromTransport);
        rulesService.importRules(encodedRules);
    }
}
