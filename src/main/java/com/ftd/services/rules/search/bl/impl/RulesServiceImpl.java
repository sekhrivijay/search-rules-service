package com.ftd.services.rules.search.bl.impl;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.commons.misc.exception.BusinessException;
import com.ftd.commons.misc.exception.InputValidationException;
import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.bl.RulesService;
import com.ftd.services.rules.search.bl.repository.RuleReloadTriggerEntity;
import com.ftd.services.rules.search.bl.repository.RuleReloadTriggerRepository;
import com.ftd.services.rules.search.bl.repository.RuleRepository;
import com.ftd.services.rules.search.config.AppConfig;
import com.ftd.services.search.api.GlobalConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;

/**
 * When an update (adds too) arrives we update the database (mongodb). We also
 * update the trigger collection on mongodb. What we don't do is actually update
 * the rule knowledgebase. This will happen on a scheduled basis rather than
 * real-time.
 */
@Service(value = "RulesService")
@EnableConfigurationProperties(AppConfig.class)
public class RulesServiceImpl implements RulesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RulesServiceImpl.class);

    private RuleRepository ruleRepository;
    private KnowledgeBaseBuilder knowledgeBaseBuilder;
    private RuleReloadTriggerRepository ruleReloadTriggerRepository;

    public RulesServiceImpl(
            @Autowired final RuleRepository ruleRepository,
            @Autowired final KnowledgeBaseBuilder knowledgeBaseBuilder,
            @Autowired final RuleReloadTriggerRepository ruleReloadTriggerRepository) {
        this.ruleRepository = ruleRepository;
        this.ruleReloadTriggerRepository = ruleReloadTriggerRepository;
        this.knowledgeBaseBuilder = knowledgeBaseBuilder;
    }

    @Override
    @Timed
    @ExceptionMetered
    public RuleEntity create(final RuleEntity ruleEntityFromClient) {
        final RuleEntity savedRule = internalCreate(ruleEntityFromClient);
        updateTimestampOnTrigger();
        return savedRule;
    }

    @Override
    @Timed
    @ExceptionMetered
    public List<RuleEntity> read() {
        return ruleRepository.findAll();
    }

    @Override
    @Timed
    @ExceptionMetered
    public List<RuleEntity> readBySiteId(final String siteId) {
        return ruleRepository.findBySiteId(siteId);
    }

    @Override
    @Timed
    @ExceptionMetered
    public List<RuleEntity> read(final RuleEntity ruleEntityFromClient) {
        return ruleRepository.findBySiteIdAndPackageNameLikeAndRuleNameLike(
                ruleEntityFromClient.getSiteId(),
                StringUtils.defaultString(ruleEntityFromClient.getPackageName(), GlobalConstants.STAR),
                StringUtils.defaultString(ruleEntityFromClient.getRuleName(), GlobalConstants.STAR));
    }

    @Override
    @Timed
    @ExceptionMetered
    public RuleEntity readById(final long id) {
        final RuleEntity ruleEntityFromDB = ruleRepository.findOne(id);
        if (ruleEntityFromDB == null) {
            return null;
        }
        return ruleEntityFromDB;
    }

    @Override
    @Timed
    @ExceptionMetered
    public RuleEntity update(final long id, final RuleEntity ruleEntityFromClient) {
        final RuleEntity ruleEntityFromDB = ruleRepository.findOne(id);
        if (ruleEntityFromDB == null) {
            throw new BusinessException(
                    HttpStatus.NOT_FOUND.name(),
                    String.format("rule %s not found in database on update", id));
        }
        ruleEntityFromClient.setId(ruleEntityFromDB.getId());
        final RuleEntity savedRule = ruleRepository.save(ruleEntityFromClient);
        updateTimestampOnTrigger();
        return savedRule;
    }

    @Override
    @Timed
    @ExceptionMetered
    public RuleEntity delete(final long id) {
        final RuleEntity ruleEntityFromDB = ruleRepository.findOne(id);
        if (ruleEntityFromDB == null) {
            throw new BusinessException(
                    HttpStatus.NOT_FOUND.name(),
                    String.format("rule %s not found in database on delete", id));
        }
        ruleRepository.delete(id);
        updateTimestampOnTrigger();
        return ruleEntityFromDB;
    }

    /**
     * This method must be called each time a rule is modified (or deleted) in the
     * database. This is a trigger for all processes that care to reload the rules
     * when they have changed. The trigger is a simple timestamp. When a process
     * finds a timestamp on this document that is greater than their locally stored
     * timestamp it is the indication the rules have changed since they were last
     * loaded.
     */
    @Timed
    @ExceptionMetered
    void updateTimestampOnTrigger() {

        RuleReloadTriggerEntity trigger = ruleReloadTriggerRepository.findById(RuleReloadTriggerRepository.TRIGGER_KEY);

        if (trigger == null) {
            trigger = new RuleReloadTriggerEntity();
            trigger.setId(RuleReloadTriggerRepository.TRIGGER_KEY);
            trigger.setLastUpdatedMS(System.currentTimeMillis());
            ruleReloadTriggerRepository.insert(trigger);
            return;
        }
        trigger.setLastUpdatedMS(System.currentTimeMillis());
        ruleReloadTriggerRepository.save(trigger);
    }

    private RuleEntity getRuleEntityFromDB(final RuleEntity ruleEntity) {
        return ruleRepository.findBySiteIdAndPackageNameAndRuleName(
                ruleEntity.getSiteId(),
                ruleEntity.getPackageName(),
                ruleEntity.getRuleName());
    }

    private void validateServiceRequest(final RuleEntity ruleServiceRequest) {
        LOGGER.debug(ruleServiceRequest.toString());
        if (StringUtils.isEmpty(ruleServiceRequest.getRuleName())
                || StringUtils.isEmpty(ruleServiceRequest.getPackageName())) {
            throw new InputValidationException(
                    HttpStatus.BAD_REQUEST.name(),
                    String.format("%s %s",
                            "Missing mandatory fields.",
                            "Both fields ruleName and packageName are required"));
        }
    }

    @Override
    @Timed
    @ExceptionMetered
    public void reloadKb() {
        knowledgeBaseBuilder.reloadRules();
    }

    @Override
    @Timed
    @ExceptionMetered
    public void importRules(final RuleEntity[] rules) {

        final List<String> errorMsgs = new ArrayList<>();
        final LongAdder lineNumber = new LongAdder();

        ruleRepository.deleteAll();

        Arrays.stream(rules).forEach(rule -> {
            try {
                lineNumber.increment();
                internalCreate(rule);
            } catch (final Exception e) {
                errorMsgs.add("import line " + lineNumber + ": " + e.getMessage());
            }
        });

        if (!errorMsgs.isEmpty()) {
            final StringBuilder sb = new StringBuilder();
            errorMsgs.stream().forEach(error -> sb.append(error).append("\n"));
            throw new InputValidationException(
                    HttpStatus.BAD_REQUEST.name(),
                    sb.toString());
        }

    }

    @Timed
    @ExceptionMetered
    RuleEntity internalCreate(final RuleEntity newRuleWithoutID) {
        validateServiceRequest(newRuleWithoutID);
        final RuleEntity ruleEntityFromDB = getRuleEntityFromDB(newRuleWithoutID);
        if (ruleEntityFromDB != null) {
            throw new InputValidationException(
                    HttpStatus.BAD_REQUEST.name(),
                    String.format("%s",
                            "A rule with same ruleName, packageName, serviceName, and environment already exists"));
        }
        return ruleRepository.insert(newRuleWithoutID);
    }

}
