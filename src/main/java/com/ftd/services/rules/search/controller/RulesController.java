package com.ftd.services.rules.search.controller;

import java.util.Base64;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.bl.RulesService;
import com.services.micro.commons.logging.annotation.LogExecutionTime;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * This class provides a restful API to maintain the drools rules. The most
 * interesting thing to note is that the rule attribute in the RuleEntity is
 * encoded to base64 during transit. This is because JSON does not handle the
 * drools rule formats very well and this was the "workaround".
 *
 */
@Api
@RestController
@RefreshScope
@RequestMapping("/{siteId}/api/rules")
public class RulesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RulesController.class);

    private RulesService        rulesService;

    public RulesController(@Autowired RulesService rulesService) {
        this.rulesService = rulesService;
    }

    private void decodeRuleFromTransport(RuleEntity ruleEntity) {
        if (ruleEntity == null || ruleEntity.getRule() == null) {
            return;
        }
        try {
            ruleEntity.setRule(new String(Base64.getDecoder().decode(ruleEntity.getRule())));
        } catch (Exception e) {
            LOGGER.warn("base64 encoding error on request", e.getMessage());
        }
    }

    private void encodeRuleForTransport(RuleEntity ruleEntity) {
        if (ruleEntity == null || ruleEntity.getRule() == null) {
            return;
        }
        try {
            ruleEntity.setRule(new String(Base64.getEncoder().encode(ruleEntity.getRule().getBytes())));
        } catch (Exception e) {
            LOGGER.warn("base64 encoding error on response", e.getMessage());
        }
    }

    /**
     * Create a Drools rule in the database (from the method comments).
     *
     * @param siteId
     *            Must be one of the allowable site ids like ftd or proflowers
     * @param ruleEntity
     *            is the model of a rule that is stored in the database
     * @return a RuleEntity that may have additional fields filled in based on the
     *         insert into the database.
     */
    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @PostMapping()
    @ApiOperation(
            value = "Create a Drools rule in the database",
            notes = "Initially created as INACTIVE and will not affect production until made ACTIVE. "
                    + "The rule in the RuleEntity must be base64 encoded. The response will also contain"
                    + " a base64 encoded rule.",
            response = RuleEntity.class)
    public ResponseEntity<RuleEntity> createRule(
            @PathVariable String siteId,
            @RequestBody RuleEntity ruleEntity) {
        decodeRuleFromTransport(ruleEntity);
        return buildResponse(rulesService.create(ruleEntity));
    }

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @PutMapping("/{id}")
    @ApiOperation(
            value = "Update a Drools rule in the database.",
            notes = "The rule in the RuleEntity must be base64 encoded. The response will also contain"
                    + " a base64 encoded rule.",
            response = RuleEntity.class)
    public ResponseEntity<RuleEntity> updateRule(
            @PathVariable String siteId,
            @PathVariable long id,
            @RequestBody RuleEntity ruleEntity) {
        decodeRuleFromTransport(ruleEntity);
        return buildResponse(rulesService.update(id, ruleEntity));
    }

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @GetMapping("/reloadKb")
    @ApiOperation(
            value = "Load the Drools rule from the database into the knowledgebase",
            notes = "",
            response = RuleEntity.class)
    public void reloadRuleKb(@PathVariable String siteId) throws Exception {
        rulesService.reloadKb();
    }

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @GetMapping("/query")
    @ApiOperation(
            value = "Find rules using a query by example",
            notes = "All attributes specified in the example parameter "
                    + "must match the rule attributes exactly to be selected."
                    + " The response will also contain base64 encoded rules.",
            response = RuleEntity.class)
    public List<RuleEntity> getRulesBy(
            @PathVariable String siteId,
            RuleEntity ruleEntity) throws Exception {
        decodeRuleFromTransport(ruleEntity);
        List<RuleEntity> ruleList = rulesService.read(ruleEntity);
        ruleList.forEach(this::encodeRuleForTransport);
        return ruleList;
    }

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @GetMapping()
    @ApiOperation(
            value = "Get the list of all rules",
            notes = "The rules that are returned are for only the siteId in the path."
                    + " The response will contain base64 encoded rules.",
            response = RuleEntity.class)
    public List<RuleEntity> getAllRules(@PathVariable String siteId) throws Exception {
        List<RuleEntity> ruleList = rulesService.readBySiteId(siteId);
        ruleList.forEach(this::encodeRuleForTransport);
        return ruleList;
    }

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @GetMapping("/{id}")
    @ApiOperation(
            value = "Find a single rule.",
            notes = " The response will contain a base64 encoded rule.",
            response = RuleEntity.class)
    public ResponseEntity<RuleEntity> getRuleById(
            @PathVariable String siteId,
            @PathVariable long id) {
        return buildResponse(rulesService.readById(id));
    }

    @Timed
    @ExceptionMetered
    @LogExecutionTime
    @DeleteMapping("/{id}")
    @ApiOperation(
            value = "Delete a specific rule",
            notes = "",
            response = RuleEntity.class)
    public ResponseEntity<RuleEntity> deleteRule(
            @PathVariable String siteId,
            @PathVariable long id) {
        return buildResponse(rulesService.delete(id));
    }

    private ResponseEntity<RuleEntity> buildResponse(RuleEntity ruleEntity) {
        if (ruleEntity == null) {
            return ResponseEntity.notFound().build();
        }
        encodeRuleForTransport(ruleEntity);
        return ResponseEntity.ok().body(ruleEntity);
    }
}
