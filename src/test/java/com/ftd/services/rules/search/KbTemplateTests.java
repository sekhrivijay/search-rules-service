package com.ftd.services.rules.search;

import java.util.Collection;

import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.junit.Assert;
import org.junit.Test;
import org.kie.api.definition.KiePackage;
import org.kie.api.definition.rule.Rule;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;

import com.ftd.services.search.api.request.SearchServiceRequest;
import com.ftd.services.search.api.response.SearchServiceResponse;

public class KbTemplateTests {

    InternalKnowledgeBase createKnowledgeBase(
            String ruleFileName,
            String packageName,
            int ruleCount)
            throws Exception {

        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(ResourceFactory.newClassPathResource(ruleFileName, getClass()), ResourceType.DRL);

        if (kbuilder.hasErrors()) {
            Assert.fail(kbuilder.getErrors().toString());
        }

        InternalKnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

        Collection<KiePackage> pkgs = kbuilder.getKnowledgePackages();
        kbase.addPackages(pkgs);
        boolean foundPackage = false;

        for (KiePackage kiePackage : pkgs) {
            if (packageName.equals(kiePackage.getName())) {
                foundPackage = true;
                Collection<Rule> rules = kiePackage.getRules();
                Assert.assertEquals("rule count", ruleCount, rules.size());
            }
        }
        Assert.assertTrue("did not find the package in the rulebase", foundPackage);
        return kbase;
    }

    @Test
    public void redirectRule() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("redirect.drl", "redirect", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setQ("rose");
        searchServiceRequest.setSiteId("ftd");
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertEquals("redirectUrl", "http://www.ftd.com/rose",
                searchServiceResponse.getRedirect().getRedirectUrl());
    }

    @Test
    public void redirectRuleNOT() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("redirect.drl", "redirect", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setQ("rose");
        searchServiceRequest.setSiteId("WONTMATCH");
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertNull("redirectUrl", searchServiceResponse.getRedirect());
    }

    @Test
    public void boostRuleNOT() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("boost.drl", "boost", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setSiteId("WONTMATCH");
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertNull("bf", searchServiceRequest.getBf());
    }

    @Test
    public void boostRuleMatchNone() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("boost.drl", "boost", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setSiteId("pro");
        searchServiceRequest.setFq(new String[] {"categories:def", "categories:jkl"});
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertNull("bf", searchServiceRequest.getBf());
    }

    @Test
    public void facet() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("facet.drl", "facet", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setSiteId("pro");
        searchServiceRequest.setFq(new String[] {"categories:XYZ", "categories:jkl"});
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();

        Assert.assertEquals("facet name 0", "occassion", searchServiceRequest.getFacetFields()[0]);
        Assert.assertEquals("facet name 1", "product", searchServiceRequest.getFacetFields()[1]);
        Assert.assertEquals("facet name 2", "price", searchServiceRequest.getFacetFields()[2]);

    }

    @Test
    public void boostRuleMatchFirst() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("boost.drl", "boost", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setSiteId("pro");
        searchServiceRequest.setFq(new String[] {"categories:XYZ", "categories:jkl"});
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertEquals("bf", "bso_order_cnt_l^2 bso_order_amt_d^1",
                searchServiceRequest.getBf());
    }

    @Test
    public void boostRuleMatchLast() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("boost.drl", "boost", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setSiteId("pro");
        searchServiceRequest.setFq(new String[] {"categories:def", "categories:XYZ"});
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertEquals("bf", "bso_order_cnt_l^2 bso_order_amt_d^1",
                searchServiceRequest.getBf());
    }

    @Test
    public void boostRuleMatchBoth() throws Exception {

        InternalKnowledgeBase kbase = createKnowledgeBase("boost.drl", "boost", 1);

        KieSession knowledgeSession = kbase.newKieSession();

        SearchServiceRequest searchServiceRequest = new SearchServiceRequest();
        searchServiceRequest.setSiteId("pro");
        searchServiceRequest.setFq(new String[] {"categories:XYZ", "categories:XYZ"});
        SearchServiceResponse searchServiceResponse = new SearchServiceResponse();

        knowledgeSession.insert(searchServiceRequest);
        knowledgeSession.insert(searchServiceResponse);
        knowledgeSession.fireAllRules();
        Assert.assertEquals("bf", "bso_order_cnt_l^2 bso_order_amt_d^1",
                searchServiceRequest.getBf());
    }
}
