package com.ftd.services.rules.search;

import static org.springframework.http.HttpStatus.OK;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Base64;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.io.Resource;
import org.kie.internal.io.ResourceFactory;
import org.mockito.Mockito;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.ftd.commons.misc.exception.BaseException;
import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.api.Status;
import com.ftd.services.rules.search.bl.repository.RuleReloadTriggerRepository;
import com.ftd.services.rules.search.bl.repository.RuleRepository;
import com.ftd.services.rules.search.config.AppConfig;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = SearchRuleServiceApplication.class)
public class SearchRuleServiceApplicationTests {

    private static final String         SITE_ID    = "JUNIT";
    private static final long           TEST_RULE_ID = 123L;

    @LocalServerPort
    private int                         port;
    private URL                         base;
    private URL                         importbase;

    @Inject
    private TestRestTemplate            template;

    @MockBean(name = "appConfig")
    private AppConfig                   appConfig;

    @MockBean
    private RuleRepository              ruleRepository;

    @MockBean
    private RuleReloadTriggerRepository ruleReloadTriggerRepository;

    @Before
    public void setUp() throws Exception {
        base = new URL("http://localhost:" + port + "/" + SITE_ID + "/api/rules");
        importbase = new URL("http://localhost:" + port + "/api/rules");
    }

    String rule(String resourceFileName) throws Exception {
        final int maxRuleSize = 4096;
        Resource rsc = ResourceFactory.newClassPathResource(resourceFileName, getClass());
        InputStream str = rsc.getInputStream();
        byte[] bytes = new byte[maxRuleSize];
        int byteCount = str.read(bytes);
        String string = new String(bytes, 0, byteCount);
        return string;
    }

    RuleEntity encoded(RuleEntity rule) throws Exception {
        RuleEntity encodedRule = new RuleEntity(rule);
        encodedRule.setRule(Base64.getEncoder().encodeToString(rule.getRule().getBytes()));
        return encodedRule;
    }

    RuleEntity decoded(RuleEntity rule) throws Exception {
        RuleEntity decodedRule = new RuleEntity(rule);
        decodedRule.setRule(new String(Base64.getDecoder().decode(rule.getRule())));
        return decodedRule;
    }

    @Test
    public void createRule() throws Exception {

        RuleEntity rule = new RuleEntity();
        rule.setId(TEST_RULE_ID);
        rule.setSiteId(SITE_ID);
        rule.setPackageName("junitPackage");
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName("junitRuleName");
        rule.setRule(rule("boost.drl"));

        Mockito.when(appConfig.getRuleStatusList()).thenReturn(Arrays.asList(new String[] {
                Status.INACTIVE.name()
        }));
        Mockito.when(ruleRepository.insert(Mockito.any(RuleEntity.class))).thenReturn(new RuleEntity(rule));
        Mockito.when(ruleRepository
                .findBySiteIdAndStatus(SITE_ID, Status.INACTIVE))
                .thenReturn(Arrays.asList(new RuleEntity[] {
                        new RuleEntity(rule)
                }));
        Mockito.when(ruleRepository.findOne(TEST_RULE_ID)).thenReturn(new RuleEntity(rule));

        ResponseEntity<RuleEntity> createResponse = template.postForEntity(
                base.toString() + "/",
                encoded(rule),
                RuleEntity.class);
        Assert.assertEquals("checking for 200 OK", OK, createResponse.getStatusCode());

        ResponseEntity<RuleEntity> findResponse = template.getForEntity(
                base.toString() + "/" + createResponse.getBody().getId(),
                RuleEntity.class);
        Assert.assertEquals("checking for 200 OK", OK, findResponse.getStatusCode());
        Assert.assertEquals("rule encoding", rule.getRule(), decoded(findResponse.getBody()).getRule());
    }

    @Test
    public void createRulesWithReload() throws Exception {

        final RuleEntity rule = new RuleEntity();
        rule.setId(TEST_RULE_ID);
        rule.setSiteId(SITE_ID);
        rule.setPackageName("junitPackage");
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName("junitRuleName");
        rule.setRule(rule("boost.drl"));

        Mockito.when(appConfig.getRuleStatusList()).thenReturn(Arrays.asList(new String[] {
                Status.INACTIVE.name()
        }));
        Mockito.when(ruleRepository.insert(Mockito.any(RuleEntity.class))).thenReturn(new RuleEntity(rule));
        Mockito.when(ruleRepository
                .findBySiteIdAndStatus(SITE_ID, Status.INACTIVE))
                .thenReturn(Arrays.asList(new RuleEntity[] {
                        new RuleEntity(rule)
                }));
        Mockito.when(ruleRepository.findOne(TEST_RULE_ID)).thenReturn(new RuleEntity(rule));

        ResponseEntity<RuleEntity> createResponse = template.postForEntity(
                base.toString() + "/",
                encoded(rule),
                RuleEntity.class);
        Assert.assertEquals("checking for 200 OK", OK, createResponse.getStatusCode());
        /*
         * reload the knowledgebase from the databases
         */
        ResponseEntity<RuleEntity> findAllResponse = template.getForEntity(
                base.toString() + "/reloadKb",
                RuleEntity.class);
        Assert.assertEquals("checking for 200 OK", OK, findAllResponse.getStatusCode());
    }

    @Test
    public void createRuleWithNoPackage() throws Exception {

        RuleEntity rule = new RuleEntity();
        rule.setId(TEST_RULE_ID);
        rule.setSiteId(SITE_ID);
        rule.setPackageName(null);
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName("junitRuleName");
        rule.setRule(rule("boost.drl"));

        Mockito.when(appConfig.getRuleStatusList()).thenReturn(Arrays.asList(new String[] {
                Status.INACTIVE.name()
        }));
        Mockito.when(ruleRepository.insert(Mockito.any(RuleEntity.class))).thenReturn(new RuleEntity(rule));
        Mockito.when(ruleRepository
                .findBySiteIdAndStatus(SITE_ID, Status.INACTIVE))
                .thenReturn(Arrays.asList(new RuleEntity[] {
                        new RuleEntity(rule)
                }));
        Mockito.when(ruleRepository.findOne(TEST_RULE_ID)).thenReturn(new RuleEntity(rule));

        ResponseEntity<BaseException> badDataResponse = template.postForEntity(
                base.toString() + "/",
                encoded(rule),
                BaseException.class);
        Assert.assertEquals("HttpStatus", HttpStatus.INTERNAL_SERVER_ERROR,
                badDataResponse.getStatusCode());
        Assert.assertEquals("checking for bad data message",
                "Missing mandatory fields. Both fields ruleName and packageName are required",
                badDataResponse.getBody().getMessage());
    }

    @Test
    public void importRules() throws Exception {

        File file = new File(getClass().getClassLoader().getResource("rules.json").getFile());
        FileReader in = new FileReader(file);

        Gson gson = new Gson();
        RuleEntity[] rules = gson.fromJson(in, RuleEntity[].class);

        Arrays.stream(rules).forEach(ruleEntity -> ruleEntity
                .setRule(new String(Base64.getEncoder().encode(ruleEntity.getRule().getBytes()))));

        ResponseEntity<BaseException> importResponse = template.postForEntity(
                importbase.toString() + "/importRules",
                rules,
                BaseException.class);

        Assert.assertEquals("checking for valid import", OK, importResponse.getStatusCode());
    }

    @Test
    public void importRuleFileJson() throws Exception {

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();

        File file = new File(getClass().getClassLoader().getResource("rules.json").getFile());

        bodyMap.add("file", new FileSystemResource(file));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        ResponseEntity<BaseException> importResponse = template.exchange(
                importbase.toString() + "/importRuleFile",
                HttpMethod.POST,
                requestEntity,
                BaseException.class);

        Assert.assertEquals("checking for valid import", OK, importResponse.getStatusCode());
    }

    @Test
    public void importRuleFileDuplicates() throws Exception {

        Mockito.when(ruleRepository
                .findBySiteIdAndPackageNameAndRuleName(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(new RuleEntity());

        MultiValueMap<String, Object> bodyMap = new LinkedMultiValueMap<>();

        File file = new File(getClass().getClassLoader().getResource("rules.json").getFile());

        bodyMap.add("file", new FileSystemResource(file));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(bodyMap, headers);

        ResponseEntity<BaseException> importResponse = template.exchange(
                importbase.toString() + "/importRuleFile",
                HttpMethod.POST,
                requestEntity,
                BaseException.class);

        Assert.assertEquals("HttpStatus", HttpStatus.INTERNAL_SERVER_ERROR,
                importResponse.getStatusCode());
        Assert.assertEquals("checking for valid import",
                "import line 1: A rule with same ruleName, packageName, serviceName, and environment already exists\n" +
                        "import line 2: A rule with same ruleName, " +
                        "packageName, serviceName, and environment already exists\n",
                importResponse.getBody().getMessage());
    }

    @Test
    public void createRuleWithNoRuleName() throws Exception {

        RuleEntity rule = new RuleEntity();
        rule.setId(TEST_RULE_ID);
        rule.setSiteId(SITE_ID);
        rule.setPackageName("junitPackage");
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName(null);
        rule.setRule(rule("boost.drl"));

        Mockito.when(appConfig.getRuleStatusList()).thenReturn(Arrays.asList(new String[] {
                Status.INACTIVE.name()
        }));
        Mockito.when(ruleRepository.insert(Mockito.any(RuleEntity.class))).thenReturn(new RuleEntity(rule));
        Mockito.when(ruleRepository.findByStatus(Status.INACTIVE)).thenReturn(Arrays.asList(new RuleEntity[] {
                new RuleEntity(rule)
        }));
        Mockito.when(ruleRepository.findOne(TEST_RULE_ID)).thenReturn(new RuleEntity(rule));

        ResponseEntity<BaseException> badDataResponse = template.postForEntity(
                base.toString() + "/",
                rule,
                BaseException.class);
        Assert.assertEquals("HttpStatus", HttpStatus.INTERNAL_SERVER_ERROR,
                badDataResponse.getStatusCode());
        Assert.assertEquals("checking for bad data message",
                "Missing mandatory fields. Both fields ruleName and packageName are required",
                badDataResponse.getBody().getMessage());
    }

    @Test
    public void createDuplicateRule() throws Exception {
        RuleEntity rule = new RuleEntity();
        rule.setSiteId(SITE_ID);
        rule.setId(TEST_RULE_ID);
        rule.setPackageName("junitPackage");
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName("junitRuleName");
        rule.setRule(rule("boost.drl"));

        Mockito.when(ruleRepository
                .findBySiteIdAndPackageNameAndRuleName(SITE_ID, rule.getPackageName(), rule.getRuleName()))
                .thenReturn(new RuleEntity(rule));

        ResponseEntity<BaseException> duplicateResponse = template.postForEntity(
                base.toString() + "/",
                rule,
                BaseException.class);
        Assert.assertEquals("HttpStatus", HttpStatus.INTERNAL_SERVER_ERROR,
                duplicateResponse.getStatusCode());
        Assert.assertEquals("checking for duplicate message",
                "A rule with same ruleName, packageName, serviceName, and environment already exists",
                duplicateResponse.getBody().getMessage());
    }

    @Test
    public void deleteRule() throws Exception {
        RuleEntity rule = new RuleEntity();
        rule.setSiteId(SITE_ID);
        rule.setId(TEST_RULE_ID);
        rule.setPackageName("junitPackage");
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName("junitRuleName");
        rule.setRule(rule("boost.drl"));

        Mockito.when(ruleRepository.findOne(TEST_RULE_ID)).thenReturn(new RuleEntity(rule));
        /*
         * delete it
         */
        HttpEntity<RuleEntity> entity = new HttpEntity<RuleEntity>(rule);
        ResponseEntity<BaseException> updateResponse = template.exchange(
                base.toString() + "/" + TEST_RULE_ID,
                HttpMethod.DELETE,
                entity,
                BaseException.class, rule);

        Assert.assertEquals("checking for 200", OK,
                updateResponse.getStatusCode());
        Assert.assertNull(updateResponse.getBody().getMessage());
    }

    @Test
    public void deleteRuleThatDoesntExist() throws Exception {

        RuleEntity rule = new RuleEntity();
        HttpEntity<RuleEntity> entity = new HttpEntity<RuleEntity>(rule);
        ResponseEntity<BaseException> updateResponse = template.exchange(
                base.toString() + "/9999",
                HttpMethod.DELETE,
                entity,
                BaseException.class, rule);

        Assert.assertEquals("HttpStatus", HttpStatus.INTERNAL_SERVER_ERROR,
                updateResponse.getStatusCode());
        Assert.assertEquals("checking for not found message",
                "rule 9999 not found in database on delete",
                updateResponse.getBody().getMessage());
    }

    @Test
    public void forceReload() throws Exception {
        ResponseEntity<RuleEntity> findResponse = template.getForEntity(
                base.toString() + "/reloadKb",
                RuleEntity.class);
        Assert.assertEquals("checking for 200", OK, findResponse.getStatusCode());
    }

    @Test
    public void updateUnknownRule() throws Exception {
        RuleEntity rule = new RuleEntity();
        rule.setSiteId(SITE_ID);
        rule.setId(TEST_RULE_ID);
        rule.setPackageName("junitPackage");
        rule.setStatus(Status.INACTIVE);
        rule.setRuleName("junitRuleName");
        rule.setRule(rule("boost.drl"));

        HttpEntity<RuleEntity> entity = new HttpEntity<RuleEntity>(encoded(rule));
        ResponseEntity<BaseException> updateResponse = template.exchange(
                base.toString() + "/1234",
                HttpMethod.PUT,
                entity,
                BaseException.class, rule);

        Assert.assertEquals("HttpStatus", HttpStatus.INTERNAL_SERVER_ERROR,
                updateResponse.getStatusCode());
        Assert.assertEquals("checking for not found message",
                "rule 1234 not found in database on update",
                updateResponse.getBody().getMessage());
    }
}
