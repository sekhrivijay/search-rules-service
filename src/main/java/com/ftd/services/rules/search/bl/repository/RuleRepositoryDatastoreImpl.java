package com.ftd.services.rules.search.bl.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.api.Status;
import com.jmethods.catatumbo.EntityManager;
import com.jmethods.catatumbo.EntityQueryRequest;
import com.jmethods.catatumbo.QueryResponse;

@Repository
public class RuleRepositoryDatastoreImpl implements RuleRepository {

    private EntityManager em;

    public RuleRepositoryDatastoreImpl(
            /*
             * from GcpServiceAccount
             */
            @Autowired EntityManager entityManager) {
        em = entityManager;
    }

    String selectAllFrom(String restOfClause) {
        StringBuilder sb = new StringBuilder();
        sb.append(selectAllFrom());
        sb.append(" ");
        sb.append(restOfClause);
        return sb.toString();
    }

    String selectAllFrom() {
        return "select * from Rule";
    }

    @Override
    public RuleEntity findBySiteIdAndPackageNameAndRuleName(String siteId, String packageName, String ruleName) {
        EntityQueryRequest request = em
                .createEntityQueryRequest(selectAllFrom("where siteId = @1 and packageName = @2 and ruleName = @3"));
        request.addPositionalBinding(siteId);
        request.addPositionalBinding(packageName);
        request.addPositionalBinding(ruleName);
        QueryResponse<RuleEntity> qr = em.executeEntityQueryRequest(RuleEntity.class, request);
        return returnOne(qr.getResults());
    }

    @Override
    public List<RuleEntity> findBySiteIdAndStatus(String siteId, Status status) {
        EntityQueryRequest request = em.createEntityQueryRequest(selectAllFrom("where siteId = @1 and status = @2"));
        request.addPositionalBinding(siteId);
        request.addPositionalBinding(status.name());
        QueryResponse<RuleEntity> qr = em.executeEntityQueryRequest(RuleEntity.class, request);
        return qr.getResults();
    }

    @Override
    public List<RuleEntity> findBySiteId(String siteId) {
        EntityQueryRequest request = em.createEntityQueryRequest(selectAllFrom("where siteId = @1"));
        request.addPositionalBinding(siteId);
        QueryResponse<RuleEntity> qr = em.executeEntityQueryRequest(RuleEntity.class, request);
        return qr.getResults();
    }

    @Override
    public List<RuleEntity> findByStatus(Status status) {
        EntityQueryRequest request = em.createEntityQueryRequest(selectAllFrom("where status = @1"));
        request.addPositionalBinding(status.name());
        QueryResponse<RuleEntity> qr = em.executeEntityQueryRequest(RuleEntity.class, request);
        return qr.getResults();
    }

    @Override
    public List<RuleEntity> findBySiteIdAndPackageNameLikeAndRuleNameLike(
            String siteId,
            String packageName,
            String ruleName) {
        EntityQueryRequest request = em.createEntityQueryRequest(
                selectAllFrom("where siteId = @1 and packageName is like @2 and ruleName is like @3"));
        request.addPositionalBinding(siteId);
        request.addPositionalBinding(packageName);
        request.addPositionalBinding(ruleName);
        QueryResponse<RuleEntity> qr = em.executeEntityQueryRequest(RuleEntity.class, request);
        return qr.getResults();
    }

    @Override
    public List<RuleEntity> findAll() {
        EntityQueryRequest request = em.createEntityQueryRequest(selectAllFrom());
        QueryResponse<RuleEntity> qr = em.executeEntityQueryRequest(RuleEntity.class, request);
        return qr.getResults();
    }

    RuleEntity returnOne(List<RuleEntity> results) {
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @Override
    public RuleEntity findOne(long id) {
        return em.load(RuleEntity.class, id);
    }

    @Override
    public RuleEntity save(RuleEntity ruleEntityFromClient) {
        return em.update(ruleEntityFromClient);
    }

    @Override
    public void delete(long id) {
        em.delete(RuleEntity.class, id);
    }

    @Override
    public void deleteAll() {
        em.deleteAll(RuleEntity.class);
    }

    @Override
    public RuleEntity insert(RuleEntity newRuleWithoutID) {
        return em.insert(newRuleWithoutID);
    }

}
