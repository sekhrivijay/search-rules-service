package com.ftd.services.rules.search.bl.repository;

import java.util.List;

import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.api.Status;

public interface RuleRepository {

    RuleEntity findBySiteIdAndPackageNameAndRuleName(
            String siteId,
            String packageName,
            String ruleName);

    List<RuleEntity> findBySiteIdAndStatus(
            String siteId,
            Status status);

    List<RuleEntity> findBySiteId(
            String siteId);

    List<RuleEntity> findByStatus(
            Status status);

    List<RuleEntity> findBySiteIdAndPackageNameLikeAndRuleNameLike(
            String siteId,
            String packageName,
            String ruleName);

    List<RuleEntity> findAll();

    RuleEntity findOne(long id);

    RuleEntity save(RuleEntity ruleEntityFromClient);

    void delete(long id);

    void deleteAll();

    RuleEntity insert(RuleEntity newRuleWithoutID);
}
