package com.ftd.services.rules.search.bl.repository;

public interface RuleReloadTriggerRepository {

    String TRIGGER_KEY = "RELOAD_TRIGGER";

    RuleReloadTriggerEntity findById(String id);

    RuleReloadTriggerEntity insert(RuleReloadTriggerEntity trigger);

    RuleReloadTriggerEntity save(RuleReloadTriggerEntity trigger);
}
