package com.ftd.services.rules.search.bl;

import com.ftd.services.search.api.rules.RuleServiceResponse;

public interface RulesExecutionService {
    RuleServiceResponse executePre(RuleServiceResponse response);

    RuleServiceResponse executePost(RuleServiceResponse response);
}
