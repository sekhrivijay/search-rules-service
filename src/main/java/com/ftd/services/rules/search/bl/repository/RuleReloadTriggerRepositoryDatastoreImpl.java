package com.ftd.services.rules.search.bl.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.jmethods.catatumbo.EntityManager;

@Repository
public class RuleReloadTriggerRepositoryDatastoreImpl implements RuleReloadTriggerRepository {

    private EntityManager em = null;

    public RuleReloadTriggerRepositoryDatastoreImpl(
            /*
             * from GcpServiceAccount
             */
            @Autowired EntityManager entityManager) {
        em = entityManager;
    }

    @Override
    public RuleReloadTriggerEntity findById(String id) {
        return em.load(RuleReloadTriggerEntity.class, id);
    }

    @Override
    public RuleReloadTriggerEntity insert(RuleReloadTriggerEntity trigger) {
        return em.insert(trigger);
    }

    @Override
    public RuleReloadTriggerEntity save(RuleReloadTriggerEntity trigger) {
        return em.update(trigger);
    }

}
