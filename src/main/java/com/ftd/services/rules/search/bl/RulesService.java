package com.ftd.services.rules.search.bl;

import java.util.List;

import com.ftd.services.rules.search.api.RuleEntity;

public interface RulesService {
    RuleEntity create(RuleEntity ruleEntity);

    List<RuleEntity> read(RuleEntity ruleEntity);

    List<RuleEntity> read();

    List<RuleEntity> readBySiteId(String siteId);

    RuleEntity readById(long id);

    RuleEntity update(long id, RuleEntity ruleEntity);

    RuleEntity delete(long id);

    void reloadKb();

    void importRules(RuleEntity[] rules);
}
