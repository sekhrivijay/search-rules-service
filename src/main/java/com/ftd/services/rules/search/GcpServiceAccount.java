package com.ftd.services.rules.search;

import com.ftd.commons.misc.exception.DataAccessException;
import com.jmethods.catatumbo.EntityManager;
import com.jmethods.catatumbo.EntityManagerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

@Service("GcpServiceAccount")
@Lazy
public final class GcpServiceAccount {

    private static final Logger LOGGER = LoggerFactory.getLogger(GcpServiceAccount.class);

    @Value("${spring.db.namespace:NONAMESPACE}")
    private String namespace;

    private File jsonCredentialsFile;
    private String projectId;

    public GcpServiceAccount(
            @Value("${spring.cloud.gcp.project-id}") final String projectId,
            @Value("${spring.cloud.gcp.credentials.location}") final String jsonCredentialsFilename) {

        this.projectId = projectId;
        jsonCredentialsFile = createCredentialsFile(jsonCredentialsFilename);
    }

    @Bean
    @Lazy
    EntityManager entityManager() {

        if ("NONAMESPACE".equals(namespace)) {
            throw new DataAccessException(HttpStatus.INTERNAL_SERVER_ERROR.name(),
                    "missing spring.db.namespace property");
        }
        return EntityManagerFactory.getInstance().createEntityManager(
                projectId,
                jsonCredentialsFile.getPath(),
                namespace);
    }

    private File createCredentialsFile(final String jsonCredentialsFilename) {
        File credFile = null;
        try {
            final Resource resource = new FileSystemResourceLoader().getResource(jsonCredentialsFilename);
            credFile = resource.getFile();
            if (!credFile.isFile()) {
                credFile = null;
                LOGGER.error("loading GCP service account credentials from {}, {}",
                        jsonCredentialsFilename,
                        "does not exist or is not a file");
            }
        } catch (final IOException e) {
            LOGGER.error("loading GCP service account credentials from {}, {}",
                    jsonCredentialsFilename,
                    e.getMessage());
        }
        return credFile;
    }
}
