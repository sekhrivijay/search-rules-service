package com.ftd.services.rules.search.bl.impl;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.commons.misc.exception.BusinessException;
import com.ftd.services.rules.search.api.RuleEntity;
import com.ftd.services.rules.search.api.Status;
import com.ftd.services.rules.search.bl.repository.RuleReloadTriggerEntity;
import com.ftd.services.rules.search.bl.repository.RuleReloadTriggerRepository;
import com.ftd.services.rules.search.bl.repository.RuleRepository;
import com.ftd.services.rules.search.config.AppConfig;
import com.ftd.services.search.api.rules.RuleServiceResponse;
import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.kie.api.definition.KiePackage;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.concurrent.atomic.LongAdder;

@Service(value = "KnowledgeBaseBuilder")
public class KnowledgeBaseBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(KnowledgeBaseBuilder.class);

    private AppConfig appConfig;
    private RuleRepository ruleRepository;
    /*
     * This is a local variable that remains valid while this instance is running.
     * It will be reinitialized from the database the next time the process starts.
     */
    private long lastReloadTimeMS = 0;
    private RuleReloadTriggerRepository ruleReloadTriggerRepository;

    private InternalKnowledgeBase knowledgeBase;

    public KnowledgeBaseBuilder(
            @Autowired final AppConfig appConfig,
            @Autowired final RuleRepository ruleRepository,
            @Autowired final RuleReloadTriggerRepository ruleReloadTriggerRepository) {
        this.appConfig = appConfig;
        this.ruleRepository = ruleRepository;
        this.ruleReloadTriggerRepository = ruleReloadTriggerRepository;
    }

    @PostConstruct
    @Timed
    @ExceptionMetered
    void prepareRuleKnowledgebaseAtStartup() {
        try {
            /*
             * Just calling the method to load the initial trigger timestamp. This will keep
             * us from unnecessarily reloading the rules database after the first trigger
             * time delay.
             */
            isRuleDatabaseModified();
            /*
             * We need to load the rules regardless of what the trigger indicates.
             */
            reloadRules();
        } catch (final Exception e) {
            LOGGER.error("initial load of the KB", e);
        }
    }

    /**
     * This method will wake up at a timed interval. All it does is read the trigger
     * document and exit if nothing has changed. Otherwise it will go ahead and
     * reload the rules. A side-effect of checking the trigger is that the current
     * trigger timestamp is saved for the next comparison.
     */
    @Timed
    @ExceptionMetered
    @Scheduled(fixedRateString = "${service.ruleReloadRate}")
    void scheduledRefresh() {
        try {
            if (isRuleDatabaseModified()) {
                reloadRules();
            }
        } catch (final Exception e) {
            LOGGER.error("scheduled refresh of the KB", e);
        }
    }

    /**
     * This method detects changes in the rule database by querying the trigger
     * document. If the timestamp on the trigger is greater than it was the last
     * time the trigger was queried then the rules have changed and must be
     * reloaded.
     * @return
     */
    @Timed
    @ExceptionMetered
    boolean isRuleDatabaseModified() {

        final RuleReloadTriggerEntity trigger = ruleReloadTriggerRepository
                .findById(RuleReloadTriggerRepository.TRIGGER_KEY);

        if (trigger == null) {
            return false;
        }
        if (trigger.getLastUpdatedMS().longValue() > lastReloadTimeMS) {
            lastReloadTimeMS = trigger.getLastUpdatedMS().longValue();
            return true;
        }
        return false;
    }

    private Resource getResource(final RuleEntity ruleEntity) {
        return ResourceFactory.newByteArrayResource(ruleEntity.getRule().getBytes());
    }

    @Timed
    @ExceptionMetered
    public void reloadRules() {
        final LongAdder rulesCount = new LongAdder();

        knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
        final KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        System.out.println(appConfig.getRuleStatusList());
        appConfig.getRuleStatusList()
                .forEach(status -> ruleRepository.findByStatus(Status.valueOf(status))
                        .forEach(ruleEntity -> {
                            rulesCount.increment();
                            kbuilder.add(getResource(ruleEntity), ResourceType.DRL);
                        }));

        if (kbuilder.hasErrors()) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST.name(),
                    kbuilder.getErrors().toString());
        }

        final Collection<KiePackage> pkgs = kbuilder.getKnowledgePackages();
        knowledgeBase.addPackages(pkgs);

        LOGGER.info("knowledgebase loaded with {} rules", rulesCount.intValue());
    }

    @Timed
    @ExceptionMetered
    public RuleServiceResponse fireRules(final RuleServiceResponse searchModelWrapper) {
        KieSession knowledgeSession = null;
        try {
            knowledgeSession = knowledgeBase.newKieSession();
            knowledgeSession.insert(searchModelWrapper.getSearchServiceRequest());
            knowledgeSession.insert(searchModelWrapper.getSearchServiceResponse());
            knowledgeSession.fireAllRules();

            LOGGER.debug("Rule execution completed ..   " + searchModelWrapper.toString());
        } catch (final Throwable t) {
            LOGGER.error("Could not execute rule ", t);
        } finally {
            if (knowledgeSession != null) {
                knowledgeSession.dispose();
            }
        }
        return searchModelWrapper;
    }

}
