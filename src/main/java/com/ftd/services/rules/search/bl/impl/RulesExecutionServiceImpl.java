package com.ftd.services.rules.search.bl.impl;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.rules.search.bl.RulesExecutionService;
import com.ftd.services.rules.search.config.AppConfig;
import com.ftd.services.search.api.rules.RuleServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;

@Service(value = "RulesExecutionService")
@EnableConfigurationProperties(AppConfig.class)
public class RulesExecutionServiceImpl implements RulesExecutionService {

    private KnowledgeBaseBuilder knowledgeBaseBuilder;

    public RulesExecutionServiceImpl(
            @Autowired final KnowledgeBaseBuilder knowledgeBaseBuilder) {
        this.knowledgeBaseBuilder = knowledgeBaseBuilder;
    }

    @Override
    @Timed
    @ExceptionMetered
    public RuleServiceResponse executePre(final RuleServiceResponse searchModelWrapper) {
        return knowledgeBaseBuilder.fireRules(searchModelWrapper);
    }

    @Override
    @Timed
    @ExceptionMetered
    public RuleServiceResponse executePost(final RuleServiceResponse searchModelWrapper) {
        return knowledgeBaseBuilder.fireRules(searchModelWrapper);
    }
}
