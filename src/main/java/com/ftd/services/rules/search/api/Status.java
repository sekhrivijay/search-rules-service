package com.ftd.services.rules.search.api;

import java.io.Serializable;

public enum Status implements Serializable {
    ACTIVE, INACTIVE;
}
