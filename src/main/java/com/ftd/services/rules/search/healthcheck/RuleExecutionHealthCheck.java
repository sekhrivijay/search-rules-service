package com.ftd.services.rules.search.healthcheck;

import com.ftd.services.rules.search.bl.RulesExecutionService;
import com.ftd.services.search.api.rules.RuleServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class RuleExecutionHealthCheck implements HealthIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(RuleExecutionHealthCheck.class);
    private static final RuleServiceResponse SEARCH_MODEL_WRAPPER =
            new RuleServiceResponse(null, null);
    private RulesExecutionService rulesExecutionService;

    @Autowired
    public void setRulesExecutionService(final RulesExecutionService rulesExecutionService) {
        this.rulesExecutionService = rulesExecutionService;
    }

    @Override
    public Health health() {
        try {
            rulesExecutionService.executePre(SEARCH_MODEL_WRAPPER);
        } catch (final Exception e) {
            LOGGER.error("Health check failed ... ", e);
            return Health
                    .down()
                    .withDetail("Error Code", 1)
                    .withException(e)
                    .build();

        }
        return Health.up().build();
    }
}
